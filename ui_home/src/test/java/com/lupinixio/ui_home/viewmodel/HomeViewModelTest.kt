package com.lupinixio.ui_home.viewmodel

import android.content.ContentResolver
import com.lupinixio.globalradio.ModuleProvider
import com.lupinixio.ui_home.HomeModule
import com.lupinixio.ui_home.model.UuidModel
import io.kotlintest.properties.verifyAll
import org.assertj.core.api.Assertions
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.koin.core.module.Module
import org.koin.dsl.module
import org.koin.test.KoinTest
import org.koin.test.inject

class HomeViewModelTest : KoinTest {

    val homeViewModel: HomeViewModel by inject()

    @Before
    fun componentInjection() {
        startKoin { modules(ModuleProvider.modules) }
        HomeModule.load()
    }

    @Test
    fun `should inject my components`() {
        homeViewModel.getNext()

        Assertions.assertThat(homeViewModel.uuid().value == UuidModel("trererewr", "reerere", 3))
    }
}