package com.lupinixio.ui_home

import com.lupinixio.ui_home.viewmodel.HomeViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.context.loadKoinModules
import org.koin.dsl.module

object HomeModule {

    fun load() = load

    private val load by lazy {
        loadKoinModules(viewModelModules)
    }

    private val viewModelModules = module {
        viewModel { HomeViewModel(get()) }
    }

}