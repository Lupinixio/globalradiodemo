package com.lupinixio.ui_home.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import com.lupinixio.abstraction.repository.GlobalRadioRepository
import com.lupinixio.ui_home.model.UuidModel
import com.lupinixio.ui_shared.base.vm.BaseViewModel
import io.reactivex.rxkotlin.subscribeBy

class HomeViewModel(private val globalRadioRepository: GlobalRadioRepository) : BaseViewModel() {

    private val uuid = MutableLiveData<UuidModel>()
    private val buttonEnabled = MediatorLiveData<Boolean>()
    private var iterationCounter = 0

    init {
        buttonEnabled.addSource(loadingInProgress) {
            buttonEnabled.value = !it
        }
    }

    fun uuid(): LiveData<UuidModel> = uuid
    fun buttonEnabled(): LiveData<Boolean> = buttonEnabled

    fun getNext(lastIteration: Int = iterationCounter) {
        val currentIteration = lastIteration + 1
        addDisposable(
            globalRadioRepository.getNext()
                .doOnSubscribe { loadingInProgress.postValue(true) }
                .doOnNext { iterationCounter = currentIteration }
                .map { UuidModel(it.path, it.responseCode, currentIteration) }
                .subscribeBy(
                    onNext = {
                        uuid.postValue(it)
                        loadingInProgress.postValue(false)
                    },
                    onError = {
                        error.postValue(it)
                        loadingInProgress.postValue(false)
                    },
                    onComplete = { loadingInProgress.postValue(false) }
                )
        )

    }
}