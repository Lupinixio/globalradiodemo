package com.lupinixio.ui_home

import android.os.Bundle
import android.view.View
import com.lupinixio.ui_shared.EntryActivity
import com.lupinixio.ui_shared.base.IActivityStatus
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : EntryActivity(), IActivityStatus {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
    }

    override fun loadModules() {
        HomeModule.load()
    }

    override fun isLoading(isLoading: Boolean) {
        progressBar.visibility = if (isLoading) View.VISIBLE else View.INVISIBLE
    }

}