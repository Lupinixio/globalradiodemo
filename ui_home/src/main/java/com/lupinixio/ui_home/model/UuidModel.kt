package com.lupinixio.ui_home.model

import com.lupinixio.abstraction.dto.Uuid

data class UuidModel(
    override val path: String,
    override val responseCode: String,
    val iteration: Int
) : Uuid