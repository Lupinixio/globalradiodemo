package com.lupinixio.ui_home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.lupinixio.ui_home.databinding.FragmentHomeBinding
import com.lupinixio.ui_home.viewmodel.HomeViewModel
import com.lupinixio.ui_shared.base.BaseFragment
import org.koin.androidx.viewmodel.ext.android.viewModel

class HomeFragment : BaseFragment<FragmentHomeBinding>() {

    private val vm: HomeViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_home, container, false
        )
        binding.lifecycleOwner = this
        binding.model = vm

        return binding.root
    }

    override fun setupInitialView() {
        setVmCallback(vm)
        binding.button.setOnClickListener { vm.getNext() }
    }

    override fun setupViewModelObservers() {

    }
}