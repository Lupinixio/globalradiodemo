package com.lupinixio.domain.ports.network

import com.lupinixio.abstraction.dto.Init
import com.lupinixio.abstraction.dto.Uuid
import io.reactivex.Single

interface GlobalRadioNetworkPort {

    fun initialFetch(): Single<Init>

    fun getUuid(path: String): Single<Uuid>
}