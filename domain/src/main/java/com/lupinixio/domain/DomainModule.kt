package com.lupinixio.domain

import com.lupinixio.abstraction.repository.GlobalRadioRepository
import com.lupinixio.domain.repository.GlobalRadioRepositoryAdapter
import org.koin.dsl.module

object DomainModule {

    private val repositoryModules = module {
        single<GlobalRadioRepository> { GlobalRadioRepositoryAdapter(get()) }
    }

    val modules = listOf(repositoryModules)
}