package com.lupinixio.domain.repository

import com.lupinixio.abstraction.dto.Uuid
import com.lupinixio.abstraction.repository.GlobalRadioRepository
import com.lupinixio.domain.ports.network.GlobalRadioNetworkPort
import com.lupinixio.domain.ports.persistence.DemoPersistencePort
import io.reactivex.Observable
import io.reactivex.rxkotlin.subscribeBy

internal class GlobalRadioRepositoryAdapter(
    private val globalRadioNetworkPort: GlobalRadioNetworkPort
) : GlobalRadioRepository {

    override fun getNext(): Observable<Uuid> {
        return globalRadioNetworkPort.initialFetch()
            .flatMap { globalRadioNetworkPort.getUuid(it.nextPath) }
            .toObservable()
    }
}