package com.lupinixio.persistence

import androidx.room.Database
import androidx.room.RoomDatabase
import com.lupinixio.persistence.dao.DemoDao
import com.lupinixio.persistence.entities.DemoEntity

object DatabaseMetaData {
    const val NAME = "demo.db"
    const val VERSION = 1
}

@Database(entities = [DemoEntity::class], version = DatabaseMetaData.VERSION)
abstract class DemoDatabase : RoomDatabase() {

    abstract fun demoDao(): DemoDao
}