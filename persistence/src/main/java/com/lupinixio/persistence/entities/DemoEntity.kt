package com.lupinixio.persistence.entities

import androidx.room.Entity
import com.lupinixio.abstraction.dto.Init

@Entity(tableName = "DemoTable", primaryKeys = ["nextPath"])
data class DemoEntity(override val nextPath: String) : Init