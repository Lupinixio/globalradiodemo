package com.lupinixio.persistence.dao

import androidx.room.Dao
import com.lupinixio.persistence.entities.DemoEntity

@Dao
interface DemoDao : BaseDao<DemoEntity> {
}