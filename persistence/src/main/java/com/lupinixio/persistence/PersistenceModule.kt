package com.lupinixio.persistence

import android.app.Application
import androidx.room.Room
import com.lupinixio.domain.ports.persistence.DemoPersistencePort
import com.lupinixio.persistence.adapters.DemoPersistenceAdapter
import org.koin.dsl.module

object PersistenceModule {

    private val databaseModule = module {
        single {
            val app: Application = get()
            Room.databaseBuilder(app, DemoDatabase::class.java, DatabaseMetaData.NAME).build()
        }
    }

    private val daoModules = module {
        single { get<DemoDatabase>().demoDao() }
    }

    private val portModules = module {
        single<DemoPersistencePort> { DemoPersistenceAdapter(get()) }
    }

    val modules = listOf(databaseModule, daoModules, portModules)
}