package com.lupinixio.persistence.adapters

import com.lupinixio.domain.ports.persistence.DemoPersistencePort
import com.lupinixio.persistence.dao.DemoDao

internal class DemoPersistenceAdapter(private val demoDao: DemoDao) : DemoPersistencePort {
}