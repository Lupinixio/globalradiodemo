package com.lupinixio.ui_shared.base

interface IActivityStatus {
    fun isLoading(isLoading: Boolean)
}