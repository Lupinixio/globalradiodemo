package com.lupinixio.ui_shared.base.vm

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class BaseViewModel : ViewModel(), IViewModelStatus {

    val TAG = javaClass.simpleName

    override val error = MutableLiveData<Throwable>()
    override val loadingInProgress = MutableLiveData<Boolean>().apply { value = false }
    override val toastMessage = MutableLiveData<String>()

    private val compositeDisposable: CompositeDisposable by lazy { CompositeDisposable() }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }

    protected fun addDisposable(disposable: Disposable) {
        compositeDisposable.add(disposable)
    }

    protected fun clearAllDisposable() {
        compositeDisposable.clear()
    }
}