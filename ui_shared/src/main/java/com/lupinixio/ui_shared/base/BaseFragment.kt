package com.lupinixio.ui_shared.base

import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.lupinixio.ui_shared.R
import com.lupinixio.ui_shared.base.vm.IViewModelStatus
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class BaseFragment<B : ViewDataBinding> : Fragment() {

    protected val navController: NavController by lazy { findNavController() }
    private var toast: Toast? = null
    private var activityStatus: IActivityStatus? = null
    private var vmStatus: IViewModelStatus? = null
    private val compositeDisposable: CompositeDisposable by lazy { CompositeDisposable() }

    val TAG = javaClass.simpleName

    protected lateinit var binding: B

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initViewModel()
        setupViewModelObservers()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        if (context is IActivityStatus)
            activityStatus = context
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupInitialView()
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.dispose()
    }

    private fun initViewModel() {
        vmStatus?.let {
            it.error.observe(this, onErrorChange())
            it.loadingInProgress.observe(this, onLoadingProgressChange())
            it.toastMessage.observe(this, onToastMessageChange())
        }
    }

    protected fun setVmCallback(statusCallback: IViewModelStatus) {
        vmStatus = statusCallback
        initViewModel()
    }

    protected abstract fun setupInitialView()

    protected abstract fun setupViewModelObservers()

    protected open fun onLoadingProgressChange(): Observer<in Boolean> {
        return Observer { activityStatus?.isLoading(it) }
    }

    protected open fun onErrorChange(): Observer<in Throwable> {
        return Observer {
            showToastMessage(it.localizedMessage ?: getString(R.string.generic_error))
        }
    }

    protected open fun onToastMessageChange(): Observer<in String> {
        return Observer { showToastMessage(it) }
    }

    protected fun showToastMessage(message: String, duration: Int = Toast.LENGTH_LONG) {
        this.toast?.let {
            it.setText(message)
            it.duration = duration
            it.show()
        } ?: run {
            toast = Toast.makeText(context, message, duration)
            toast!!.show()
        }

    }

    protected fun addDisposable(disposable: Disposable) {
        compositeDisposable.add(disposable)
    }

    protected fun clearAllDisposable() {
        compositeDisposable.clear()
    }


}