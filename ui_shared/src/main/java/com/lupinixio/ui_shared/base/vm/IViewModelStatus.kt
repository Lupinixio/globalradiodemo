package com.lupinixio.ui_shared.base.vm

import androidx.lifecycle.MutableLiveData

interface IViewModelStatus {
    val error: MutableLiveData<Throwable>
    val loadingInProgress: MutableLiveData<Boolean>
    val toastMessage: MutableLiveData<String>
}