package com.lupinixio.globalradio

import android.content.ContentResolver
import com.lupinixio.domain.DomainModule
import com.lupinixio.network.NetworkModule
import com.lupinixio.persistence.PersistenceModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.module.Module
import org.koin.dsl.module

object ModuleProvider {

    private val appModules = module {
        single<ContentResolver> { androidContext().contentResolver }
    }

    val modules: List<Module>
        get() {
            return ArrayList<Module>().apply {
                add(appModules)
                addAll(DomainModule.modules)
                addAll(PersistenceModule.modules)
                addAll(NetworkModule.modules)
            }
        }
}