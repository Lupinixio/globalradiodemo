package com.lupinixio.globalradio

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.lupinixio.navigation.HomeNavigation

class LauncherActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        HomeNavigation.dynamicStart?.let { startActivity(it) }
        finish()
    }
}
