package com.lupinixio.network.adapters

import com.lupinixio.abstraction.dto.Init
import com.lupinixio.abstraction.dto.Uuid
import com.lupinixio.domain.ports.network.GlobalRadioNetworkPort
import com.lupinixio.network.webservice.GlobalRadioWebService
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers

internal class GlobalRadioNetworkAdapter(private val webService: GlobalRadioWebService) :
    GlobalRadioNetworkPort {

    override fun initialFetch(): Single<Init> {
        return webService.startFetching()
            .map { it as Init }
            .subscribeOn(Schedulers.io())
    }

    override fun getUuid(path: String): Single<Uuid> {
        return webService.getPath(path)
            .map { it as Uuid }
            .subscribeOn(Schedulers.io())
    }
}