package com.lupinixio.network.webservice

import com.lupinixio.network.model.InitResponse
import com.lupinixio.network.model.UuidResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Url

interface GlobalRadioWebService {

    @GET(".")
    fun startFetching(): Single<InitResponse>

    @GET
    fun getPath(@Url path: String): Single<UuidResponse>
}