package com.lupinixio.network.model

import com.google.gson.annotations.SerializedName
import com.lupinixio.abstraction.dto.Uuid

data class UuidResponse(
    @SerializedName("path")
    override val path: String,
    @SerializedName("response_code")
    override val responseCode: String
) : Uuid