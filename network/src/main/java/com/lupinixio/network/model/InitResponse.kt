package com.lupinixio.network.model

import com.google.gson.annotations.SerializedName
import com.lupinixio.abstraction.dto.Init

data class InitResponse(
    @SerializedName("next_path")
    override val nextPath: String
) : Init