package com.lupinixio.network

import com.lupinixio.domain.ports.network.GlobalRadioNetworkPort
import com.lupinixio.network.adapters.GlobalRadioNetworkAdapter
import com.lupinixio.network.webservice.GlobalRadioWebService
import okhttp3.CertificatePinner
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.net.Proxy
import java.util.concurrent.TimeUnit

object NetworkModule {

    private val retrofitModule = module {
        single {
            buildRetrofitClient(
                buildOkHttpClient(),
                "http://10.0.2.2:8000/"
            )
        }
    }

    private val webServiceModules = module {
        single { get<Retrofit>().create(GlobalRadioWebService::class.java) }
    }

    private val portModules = module {
        single<GlobalRadioNetworkPort> { GlobalRadioNetworkAdapter(get()) }
    }

    private fun buildOkHttpClient(
        proxy: Proxy? = Proxy.NO_PROXY,
        pinner: CertificatePinner? = null,
        interceptor: Interceptor? = null
    ): OkHttpClient {
        val client = OkHttpClient.Builder()
            .proxy(proxy)

        pinner?.let { client.certificatePinner(it) }
        interceptor?.let { client.addInterceptor(it) }

        client.addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
            .proxy(null)

        return client.build()
    }

    private fun buildRetrofitClient(okHttpClient: OkHttpClient, url: String): Retrofit {
        return Retrofit.Builder()
            .baseUrl(url)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(okHttpClient)
            .build()
    }

    val modules = listOf(
        retrofitModule,
        webServiceModules, portModules
    )

}