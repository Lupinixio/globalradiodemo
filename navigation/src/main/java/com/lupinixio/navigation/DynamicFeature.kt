package com.lupinixio.navigation

interface DynamicFeature<T> {
    val dynamicStart: T?
}