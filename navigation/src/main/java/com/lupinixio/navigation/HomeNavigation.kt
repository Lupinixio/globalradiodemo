package com.lupinixio.navigation

import android.content.Intent
import com.lupinixio.navigation.loaders.loadIntentOrNull

object HomeNavigation : DynamicFeature<Intent> {

    private const val HOME = "com.lupinixio.ui_home.HomeActivity"

    override val dynamicStart: Intent?
        get() = HOME.loadIntentOrNull()
}