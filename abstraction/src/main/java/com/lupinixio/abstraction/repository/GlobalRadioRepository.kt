package com.lupinixio.abstraction.repository

import com.lupinixio.abstraction.dto.Init
import com.lupinixio.abstraction.dto.Uuid
import io.reactivex.Observable

interface GlobalRadioRepository {

    fun getNext(): Observable<Uuid>
}