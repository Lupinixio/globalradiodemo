package com.lupinixio.abstraction.dto

interface Uuid {

    val path: String
    val responseCode: String
}